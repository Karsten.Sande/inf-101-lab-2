package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    Integer healthPoints;
    Integer maxHealthPoints;
    Integer strength;
    Random random;


    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints =  this.healthPoints;
        this.strength = (int) Math.abs((Math.round(20 + 10 * random.nextGaussian())));
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        return (this.healthPoints > 0); 
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(String.format("%s attacks %s.", this.name, target));
        target.damage(damageInflicted);
        
        if (!target.isAlive()) {
            System.out.println(String.format("%s is defeated by %s.", target, this.name));
        }

    }

    @Override
    public void damage(int damageTaken) {

        this.healthPoints  =Math.max(this.healthPoints - damageTaken, 0);
        System.out.println(String.format("%s takes %s damage and is left with %s/%s HP", this.name, damageTaken, Math.max(this.healthPoints, 0), this.maxHealthPoints));
    }

    @Override
    public String toString() {
        return String.format("%s HP: (%s/%s) STR: %s", this.name, this.healthPoints, this.maxHealthPoints, this.strength);
    }

}
